﻿/*======================================
 作者：洞庭夕照
 创建：2017.04.18
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 论坛：bbs.ninesky.cn
 版本：v1.0.0.0
 =====================================*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ninesky.Models
{
    /// <summary>
    /// 内容模型
    /// </summary>
    public class Content
    {
        [Key]
        public int ContentId { get; set; }
        
        /// <summary>
        /// 所属栏目
        /// </summary>
        [Required(ErrorMessage ="必须输入{0}")]
        [Display(Name ="所属栏目")]
        public int CategoryId { get; set; }

        /// <summary>
        /// 栏目名称
        /// </summary>
        [Display(Name = "栏目名称")]
        [NotMapped]
        public string CategoryName { get; set; }

        /// <summary>
        /// 模块类型
        /// </summary>
        [Required(ErrorMessage = "必须输入{0}")]
        [Display(Name = "模块类型")]
        public ModuleType ModuleType { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Required(ErrorMessage = "必须输入{0}")]
        [StringLength(255,ErrorMessage ="必须少于{1}个字符")]
        [Display(Name = "标题")]
        public string Title { get; set; }

        /// <summary>
        ///录入者 
        /// </summary>
        public string Inputer { get; set; }

        /// <summary>
        /// 点击数
        /// </summary>
        [Display(Name = "点击数")]
        public int Hits { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Name = "创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Display(Name = "状态")]
        public ContentStatus Status { get; set; }

        /// <summary>
        /// 首页图片
        /// </summary>
        [StringLength(255, ErrorMessage = "必须少于{1}个字符")]
        [Display(Name = "首页图片")]
        public string DefaultPicUrl { get; set; }

        /// <summary>
        /// 链接地址
        /// </summary>
        [StringLength(255, ErrorMessage = "必须少于{1}个字符")]
        [Display(Name = "链接地址")]
        public string LinkUrl { get; set; }

        /// <summary>
        /// 文章
        /// </summary>
        [NotMapped]
        public Article Article { get; set; }

    }

    /// <summary>
    /// 内容状态
    /// </summary>
    public enum ContentStatus {
        /// <summary>
        /// 草稿
        /// </summary>
        [Display(Name = "草稿")]
        Draft,
        /// <summary>
        /// 退稿
        /// </summary>
        [Display(Name = "退稿")]
        Rejection,
        /// <summary>
        /// 删除
        /// </summary>
        [Display(Name = "已删除")]
        Deleted,
        /// <summary>
        /// 未审核
        /// </summary>
        [Display(Name = "未审核")]
        Revised,
        /// <summary>
        /// 正常
        /// </summary>
        [Display(Name = "正常")]
        Normal=99
    }
}
