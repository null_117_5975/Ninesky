﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ninesky.Web.Migrations
{
    public partial class upcontent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArticleContent",
                table: "Articles",
                newName: "Content");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "Modules",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ModuleType",
                table: "Contents",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Modules");

            migrationBuilder.DropColumn(
                name: "ModuleType",
                table: "Contents");

            migrationBuilder.RenameColumn(
                name: "Content",
                table: "Articles",
                newName: "ArticleContent");
        }
    }
}
