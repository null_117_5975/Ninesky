﻿function ContnetStatusToString(key)
{
    var rts = "-";
    switch (key)
    {
        case 0:
            rts = "草稿";
            break;
        case 1:
            rts = "退稿"
            break;
        case 2:
            rts = "已删除";
            break;
        case 3:
            rts = "未审核";
            break;
        case 99:
            rts = "正常";
            break;
    }
    return rts;
}

function StringToContnetStatus(key) {
    var rts = "";
    switch (key) {
        case "草稿":
            rts = 0;
            break;
        case "退稿":
            rts = 1
            break;
        case "已删除":
            rts = 2;
            break;
        case "未审核":
            rts = 3;
            break;
        case "正常":
            rts = 99;
            break;
    }
    return rts;
}