﻿
$(document).ready(function () {
    //标签切换
    $('#contentab').on('shown.bs.tab', function (e) {
        $("#contentab").attr("data-ns-value", StringToContnetStatus($(e.target).text()));
        $('#contenttable').bootstrapTable('refresh');
    })
    //表格
    $('#contenttable').bootstrapTable({
        showRefresh: true,
        showColumns: true,
        pagination: true,
        toolbar: '#contentToolbar',
        sidePagination: 'server',
        pageList: [10, 20, 50, 100],
        columns: [{
            field: 'state',
            checkbox: true
        },
        {
            field: 'contentId',
            title: 'ID'
        }, {
            field: 'title',
            title: '标题',
            formatter: function (value, row, index) {
                return '[<a href="' + $('#contenttable').attr("data-ns-categoryurl") + '/' + row.categoryId + '">' + row.categoryName + '</a>]<a href="' + $('#contenttable').attr("data-ns-url") + '/' + row.contentId + '">' + value + '</a>';
            }
        }, {
            field: 'inputer',
            title: '录入人'
        }, {
            field: 'hits',
            title: '点击数'
        }, {
            field: 'createTime',
            title: '创建时间'
        }, {
            field: 'status',
            title: '状态',
            formatter: function (value, row, index) {
                return ContnetStatusToString(value);
            }
        }],
        queryParams: function () {
            return {
                contentId: $("#contentId").val(),
                title: $("#title").val(),
                inputer: $("#inputer").val(),
                order: $("#order").val(),
                status: $("#contentab").attr("data-ns-value"),
                categoryId: $("#contenttable").attr("data-ns-categoryid")
            };
        }
    });
})