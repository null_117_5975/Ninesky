﻿var categoryTree;
var asidecategorysetting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "pId",
            rootPId: 0
        }
    },
    async: {
        enable: true,
        url: $("#categoryTree").attr("data-url")
    },
    callback: {
        onAsyncSuccess: function (event, treeId, treeNode, msg) {
            categoryTree.expandAll(true);
        }
    }
};

$(document).ready(function () {
    categoryTree = $.fn.zTree.init($("#categoryTree"), asidecategorysetting);
});