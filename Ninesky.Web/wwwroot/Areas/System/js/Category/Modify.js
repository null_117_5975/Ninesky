﻿//下拉栏目设置
var dropdownCategoryTree;
var setting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "pId",
            rootPId: 0
        }
    },
    async: {
        enable: true,
        url: $("#ParentId").attr("data-ns-url")
    },
    callback: {
        onClick: function (event, treeId, treeNode) {
            $("#ParentId").val(treeNode.id);
            $("#ParentId-text").val(treeNode.name);
            $("#ParentId-dropdown").hide();
        },
        onAsyncSuccess: function (event, treeId, treeNode, msg) {
            var node = dropdownCategoryTree.getNodeByParam("id", $('#ParentId').val(), null);
            dropdownCategoryTree.selectNode(node);
            dropdownCategoryTree.expandAll(true);
            $('#ParentId-text').val(node.name);
        }
    }
};

$(document).ready(function () {
    dropdownCategoryTree = $.fn.zTree.init($("#ParentId-dropdown"), setting);
    dropdownCategoryTree.addNodes(null, { id: 0, name: "无" });
    $("#ParentId-text").click(function () {
        $("#ParentId-dropdown").show();
    });
    $("#ParentId-btn").click(function () {
        $("#ParentId-dropdown").show();
    });
    
    //删除事件
    $("#btn-del").click(function () {
        BootstrapDialog.confirm({
            title: '提示',
            message: '确认删除栏目',
            type: BootstrapDialog.TYPE_DANGER,
            closable: true,
            draggable: true,
            btnOKLabel: '确定',
            btnCancelLabel: '取消',
            btnOKClass: 'btn-danger',
            callback: function (result) {
                if (result) {
                    $.post($("#btn-del").attr("data-ns-action"), null, function (data) {
                        if (data != undefined) {
                            if (data.succeed == true) {
                                BootstrapDialog.alert({ title: "成功", message: data.message, callback: function () { location.href = "/System/Category"; } });
                            }
                            else BootstrapDialog.alert({ title: "失败", message: data.message });
                        }
                        else BootstrapDialog.alert({ title: "消息", message: "请求失败" });
                    }, 'json');
                }
            }
        });

    });
});