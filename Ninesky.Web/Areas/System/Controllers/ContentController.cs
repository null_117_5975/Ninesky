/*======================================
 作者：洞庭夕照
 创建：2017.04.18
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 论坛：bbs.ninesky.cn
 版本：v1.0.0.0
 =====================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ninesky.InterfaceBase;
using Ninesky.Models;

namespace Ninesky.Web.Areas.System.Controllers
{
    /// <summary>
    /// 内容控制器
    /// </summary>
    [Area("system")]
    public class ContentController : Controller
    {
        private InterfaceContentService _contentService;

        public ContentController(InterfaceContentService contentService)
        {
            _contentService = contentService;
        }
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        ///内容列表 
        /// </summary>
        /// <returns></returns>
        public IActionResult List()
        {
            return View();
        }

        /// <summary>
        /// 修改内容
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Modify(int id)
        {
            var moduleType = await _contentService.FindModuleTypeAsync(id);
            switch(moduleType)
            {
                case ModuleType.Article:
                return View("ModifyArticle", await _contentService.FindAsync(id));
                default:
                    return Content("内容模型错误！");
            }
            
        }

        /// <summary>
        /// 查找Ajax
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="inputer">录入者</param>
        /// <param name="contentId">内容Id</param>
        /// <param name="categoryId">栏目Id</param>
        /// <param name="order">排序方式</param>
        /// <returns></returns>
        public async Task<JsonResult> SearchAjaxAsync(int? status,string title, string inputer, int contentId = 0, int categoryId = 0, int pageIndex = 1, int pageSize = 20, int order = 0)
        {
            Paging<Content> contentPaging;
            if(status == null) contentPaging = await _contentService.FindListAsync(null, title, inputer, contentId, categoryId, pageIndex, pageSize, (ContentOrder)order);
            else contentPaging = await _contentService.FindListAsync((ContentStatus)status, title, inputer, contentId, categoryId, pageIndex, pageSize, (ContentOrder)order);
            return Json(new { total = contentPaging.Total, rows = contentPaging.Entities });
        }
    }
}