﻿/*======================================
 作者：洞庭夕照
 创建：2016.12.28
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 版本：v1.0.0.0
 =====================================*/
using Ninesky.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Ninesky.InterfaceBase
{
    /// <summary>
    /// 模块接口
    /// </summary>
    public interface InterfaceModuleService
    {
        /// <summary>
        /// 查找模块
        /// </summary>
        /// <param name="id">模块Id</param>
        /// <returns></returns>
        Task<Module> FindAsync(int id);

        /// <summary>
        /// 查找列表
        /// </summary>
        /// <returns></returns>
        Task<IQueryable<Module>> FindListAsync();

        /// <summary>
        /// 查找列表
        /// </summary>
        /// <param name="enable">启用</param>
        /// <returns></returns>
        Task<IQueryable<Module>> FindListAsync(bool? enable);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="module">模块</param>
        /// <returns></returns>
        Task<OperationResult> UpdateAsync(Module module);
    }
}
