﻿/*======================================
 作者：洞庭夕照
 创建：2017.04.19
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 论坛：bbs.ninesky.cn
 版本：v1.0.0.0
 =====================================*/
using Microsoft.EntityFrameworkCore;
using Ninesky.InterfaceBase;
using Ninesky.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ninesky.DAL;
using Ninesky.InterfaceDAL;

namespace Ninesky.Base
{
    /// <summary>
    /// 栏目服务类
    /// </summary>
    public class ContentService : InterfaceContentService
    {
        private InterfaceBaseManager<Content> _contentManager;
        private InterfaceBaseManager<Article> _articleMabager;
        public ContentService(DbContext dbContext)
        {
            _contentManager = new BaseManager<Content>(dbContext);
            _articleMabager = new BaseManager<Article>(dbContext);
        }

        /// <summary>
        /// 查找内容[含外键]
        /// </summary>
        /// <param name="id">内容Id</param>
        /// <returns></returns>
        public async Task<Content> FindAsync(int id)
        {
            Content content =  await _contentManager.FindAsync(id);
            if(content!= null)
            {
                switch(content.ModuleType)
                {
                    case ModuleType.Article:
                        content.Article = await FindArticleAsync(id);
                        break;
                }
            }
            return content;
        }

        /// <summary>
        /// 查找文章
        /// </summary>
        /// <param name="id">内容Id</param>
        /// <returns></returns>
        public async Task<Article> FindArticleAsync(int id)
        {
            return await _articleMabager.FindAsync(a => a.ContentId == id);
        }

        /// <summary>
        /// 查找模块类型
        /// </summary>
        /// <param name="id">内容Id</param>
        /// <returns></returns>
        public async Task<ModuleType> FindModuleTypeAsync(int id)
        {
            return (await _contentManager.FindAsync(id)).ModuleType;
        }

        /// <summary>
        /// 查找内容列表
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="inputer">录入者</param>
        /// <param name="contentId">内容Id</param>
        /// <param name="categoryId">栏目Id</param>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">每页记录数</param>
        /// <param name="orderType">排序</param>
        /// <returns></returns>
        public async Task<Paging<Content>> FindListAsync(ContentStatus? status, string title, string inputer, int contentId = 0, int categoryId = 0, int pageIndex = 1, int pageSize = 20, ContentOrder orderType = ContentOrder.IdDesc)
        {
            Paging<Content> paging = new Paging<Content>() { PageIndex = pageIndex, PageSize = pageSize };
            var contents = await _contentManager.FindListAsync();
            if (status != null) contents = contents.Where(c => c.Status == status);
            if (!string.IsNullOrEmpty(title)) contents = contents.Where(c => c.Title.Contains(title));
            if (!string.IsNullOrEmpty(inputer)) contents = contents.Where(c => c.Inputer.Contains(inputer));
            if (contentId > 0) contents = contents.Where(c => c.ContentId == contentId);
            if (categoryId > 0) contents = contents.Where(c => c.CategoryId == categoryId);
            paging.Total = await contents.CountAsync();
            switch (orderType)
            {
                case ContentOrder.IdAsc:
                    contents = contents.OrderBy(c => c.ContentId);
                    break;
                case ContentOrder.IdDesc:
                    contents = contents.OrderByDescending(c => c.ContentId);
                    break;
                case ContentOrder.UpdatedAsc:
                    contents = contents.OrderBy(c => c.CreateTime);
                    break;
                case ContentOrder.UpdatedDesc:
                    contents = contents.OrderByDescending(c => c.CreateTime);
                    break;
                case ContentOrder.HitsAsc:
                    contents = contents.OrderBy(c => c.Hits);
                    break;
                case ContentOrder.HitsDesc:
                    contents = contents.OrderByDescending(c => c.Hits);
                    break;
            }
            contents = contents.GroupJoin(_contentManager.DbContext.Set<Category>().AsQueryable(), ct => ct.CategoryId, cg => cg.CategoryId, (ct, cg) => new Content()
            {
                CategoryId = ct.CategoryId,
                CategoryName = cg.FirstOrDefault(x => x.CategoryId == ct.CategoryId).Name,
                ContentId = ct.ContentId,
                CreateTime = ct.CreateTime,
                DefaultPicUrl = ct.DefaultPicUrl,
                Hits = ct.Hits,
                Inputer = ct.Inputer,
                LinkUrl = ct.LinkUrl,
                Status = ct.Status,
                Title = ct.Title
            }).Select(o => o);
            paging.Entities = await contents.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).ToListAsync();
            return paging;
        }
    }
}
