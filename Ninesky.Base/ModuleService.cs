﻿/*======================================
 作者：洞庭夕照
 创建：2016.12.28
 网站：www.ninesky.cn
       mzwhj.cnblogs.com
 代码：git.oschina.net/ninesky/Ninesky
 版本：v1.0.0.0
 =====================================*/
using Microsoft.EntityFrameworkCore;
using Ninesky.InterfaceBase;
using Ninesky.Models;
using System.Linq;
using System.Threading.Tasks;
using Ninesky.InterfaceDAL;
using Ninesky.DAL;

namespace Ninesky.Base
{
    public class ModuleService: InterfaceModuleService
    {
        private InterfaceBaseManager<Module> _moduleManager;
        public ModuleService(DbContext dbContext)
        {
            _moduleManager = new BaseManager<Module>(dbContext);
        }

        public async Task<Module> FindAsync(int id)
        {
            return await _moduleManager.FindAsync(id);
        }

        /// <summary>
        /// 查找列表
        /// </summary>
        /// <returns></returns>
        public async Task<IQueryable<Module>> FindListAsync()
        {
            return await _moduleManager.FindListAsync();
        }

        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="enable">启用</param>
        /// <returns></returns>
        public async Task<IQueryable<Module>> FindListAsync(bool? enable)
        {
            if (enable == null) return await _moduleManager.FindListAsync();
            else return await _moduleManager.FindListAsync(m => m.Enabled == enable);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="module">模块</param>
        /// <returns></returns>
        public async Task<OperationResult> UpdateAsync(Module module)
        {
            return await _moduleManager.UpdateAsync(module);
        }
    }
}
